# Gilded Rose starting position in Java

https://kata-log.rocks/gilded-rose-kata

## Run the Text Fixture from Command-Line

```
./gradlew -q text
```

### Specify Number of Days

For e.g. 10 days:

```
./gradlew -q text --args 10
```
