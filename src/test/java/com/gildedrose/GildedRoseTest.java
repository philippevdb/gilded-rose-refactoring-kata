package com.gildedrose;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GildedRoseTest {

    @Nested
    class DefaultScenario {
        @Test
        void whenBothDecrease() {
            Item[] items = new Item[] { new Item("foo", 1, 10) };
            GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals("foo", app.items[0].name);
            assertEquals(0, app.items[0].sellIn);
            assertEquals(9, app.items[0].quality);
        }
        @Test
        void whenMinimumZeroReached() {
            Item[] items = new Item[] { new Item("foo", 0, 0) };
            GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals("foo", app.items[0].name);
            assertEquals(-1, app.items[0].sellIn);
            assertEquals(0, app.items[0].quality);
        }
       @Test
        void whenSellInDateHasPassed() {
            Item[] items = new Item[] {
                new Item("foo", 0, 10),
                new Item("foo", 0, 1)
            };
            GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals("foo", app.items[0].name);
            assertEquals(-1, app.items[0].sellIn);
            assertEquals(8, app.items[0].quality);
            assertEquals("foo", app.items[1].name);
            assertEquals(-1, app.items[1].sellIn);
            assertEquals(0,app.items[1].quality);
        }
    }



    @Nested
    class AgedBrie {
        @Test
        void whenAgingQualityIncreases() {
            Item[] items = new Item[] {new Item("Aged Brie", 10, 10)};
            GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals("Aged Brie", app.items[0].name);
            assertEquals(9, app.items[0].sellIn);
            assertEquals(11, app.items[0].quality); // increases when aging
        }

        @ParameterizedTest(name = "when max value reached, quality={0}")
        @ValueSource(ints = {49,50})
        void whenMaxValueReached(int quality) {
            Item[] items = new Item[] {new Item("Aged Brie", 10, quality)};
            GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals("Aged Brie", app.items[0].name);
            assertEquals(9, app.items[0].sellIn);
            assertEquals(50,app.items[0].quality); // maximum value
        }

        @Test
        void whenSellInDateHasPast() {
            Item[] items = new Item[] {new Item("Aged Brie", 0, 10)};
            GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals("Aged Brie", app.items[0].name);
            assertEquals(-1, app.items[0].sellIn);
            assertEquals(12,app.items[0].quality);
        }
    }

    @Nested
    class Sulfuras {

        @Test
        void whenImmutable() {
            Item[] items = new Item[] {new Item("Sulfuras, Hand of Ragnaros", 80, 80)};
            GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals("Sulfuras, Hand of Ragnaros", app.items[0].name);
            assertEquals(80, app.items[0].sellIn);
            assertEquals(80,app.items[0].quality);
        }
    }

    @Nested
    class BackStagePasses {
        @Test
        void whenAgingQualityIncreases() {
            Item[] items = new Item[] {new Item("Backstage passes to a TAFKAL80ETC concert", 25, 25)};
            GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals("Backstage passes to a TAFKAL80ETC concert", app.items[0].name);
            assertEquals(24, app.items[0].sellIn);
            assertEquals(26,app.items[0].quality);
        }

        @Test
        void whenTenDaysOrLessToSell() {
            Item[] items = new Item[] {
                new Item("Backstage passes to a TAFKAL80ETC concert", 10, 25),
                new Item("Backstage passes to a TAFKAL80ETC concert", 6, 25)
            };
            GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals("Backstage passes to a TAFKAL80ETC concert", app.items[0].name);
            assertEquals(9, app.items[0].sellIn);
            assertEquals(27,app.items[0].quality);
            assertEquals("Backstage passes to a TAFKAL80ETC concert", app.items[1].name);
            assertEquals(5, app.items[1].sellIn);
            assertEquals(27,app.items[1].quality);
        }
        @Test
        void whenFiveDaysOrLessToSell() {
            Item[] items = new Item[] {
                new Item("Backstage passes to a TAFKAL80ETC concert", 5, 25),
                new Item("Backstage passes to a TAFKAL80ETC concert", 1, 25)
            };
            GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals("Backstage passes to a TAFKAL80ETC concert", app.items[0].name);
            assertEquals(4, app.items[0].sellIn);
            assertEquals(28,app.items[0].quality);
            assertEquals("Backstage passes to a TAFKAL80ETC concert", app.items[1].name);
            assertEquals(0, app.items[1].sellIn);
            assertEquals(28,app.items[1].quality);
        }

        @ParameterizedTest(name = "when concert has past, quality={0}")
        @ValueSource(ints = {10,20,30,40,50})
        void whenConcertHasPastNoValue(int quality) {
            Item[] items = new Item[] {new Item("Backstage passes to a TAFKAL80ETC concert", 0, quality)};
            GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals("Backstage passes to a TAFKAL80ETC concert", app.items[0].name);
            assertEquals(-1, app.items[0].sellIn);
            assertEquals(0,app.items[0].quality);
        }

        @ParameterizedTest(name = "when max value reached, sellIn={0} quality={1}")
        @CsvSource({"11,50","10,49","5,48"})
        void whenMaxValueReached(int sellIn, int quality) {
            Item[] items = new Item[] {new Item("Backstage passes to a TAFKAL80ETC concert", sellIn, quality)};
            GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals("Backstage passes to a TAFKAL80ETC concert", app.items[0].name);
            assertEquals(50,app.items[0].quality); // maximum value
        }
    }

    @Nested
    class Conjured {
        @Test
        void whenDecreasingAtDoubleRate() {
            Item[] items = new Item[] {new Item("Conjured", 25, 25)};
            GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals("Conjured", app.items[0].name);
            assertEquals(24, app.items[0].sellIn);
            assertEquals(23,app.items[0].quality);
        }
        @Test
        void whenSellInDatePast() {
            Item[] items = new Item[] {new Item("Conjured", 0, 25)};
            GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals("Conjured", app.items[0].name);
            assertEquals(-1, app.items[0].sellIn);
            assertEquals(21,app.items[0].quality);
        }
        @ParameterizedTest(name = "when minimum reached, quality={0}")
        @ValueSource(ints = {0,1,2,3,4})
        void whenMinimumReached(int quality) {
            Item[] items = new Item[] {new Item("Conjured", 0, quality)};
            GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals("Conjured", app.items[0].name);
            assertEquals(-1, app.items[0].sellIn);
            assertEquals(0,app.items[0].quality);
        }
    }
}
