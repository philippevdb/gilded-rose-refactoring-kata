package com.gildedrose;

public class AgedBrieStrategy extends InventoryLifeTemplate {

    @Override
    protected void updateQualityBeforeAging(Item item) {
        increaseQuality(item);
    }

    @Override
    protected void updateQualityAfterAging(Item item) {
        increaseQuality(item);
    }
}
