package com.gildedrose;

public class ConjuredStrategy extends InventoryLifeTemplate {

    @Override
    protected void updateQualityBeforeAging(Item item) {
        decreaseQuality(item);
        decreaseQuality(item);
    }

    @Override
    protected void updateQualityAfterAging(Item item) {
        decreaseQuality(item);
        decreaseQuality(item);
    }
}
