package com.gildedrose;

public class BackStagePassesStrategy extends InventoryLifeTemplate {

    @Override
    protected void updateQualityBeforeAging(Item item) {
        increaseQuality(item);
        if (item.sellIn < 11) {
            increaseQuality(item);
        }
        if (item.sellIn < 6) {
            increaseQuality(item);
        }
    }

    @Override
    protected void updateQualityAfterAging(Item item) {
        item.quality = 0;
    }
}
