package com.gildedrose;

public class SulfurasStrategy extends InventoryLifeTemplate {

    @Override
    protected void updateQualityBeforeAging(Item item) {
        // do nothing
    }

    protected void ageItem(Item item) {
        // do nothing
    }

    @Override
    protected void updateQualityAfterAging(Item item) {
        // do nothing
    }
}
