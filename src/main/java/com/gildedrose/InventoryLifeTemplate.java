package com.gildedrose;

public abstract class InventoryLifeTemplate {

    protected static final int MAX_QUALITY = 50;
    protected static final int MIN_QUALITY = 0;
    protected static final int SELL_DATE = 0;

    public void updateQuality(Item item) {
        updateQualityBeforeAging(item);
        ageItem(item);
        if (item.sellIn < SELL_DATE) {
            updateQualityAfterAging(item);
        }
    }

    abstract void updateQualityBeforeAging(Item item);

    protected void ageItem(Item item) {
        item.sellIn -= 1;
    }

    abstract void updateQualityAfterAging(Item item);

    protected void increaseQuality(Item item) {
        if (item.quality < MAX_QUALITY) {
            item.quality += 1;
        }
    }

    protected void decreaseQuality(Item item) {
        if (item.quality > MIN_QUALITY) {
            item.quality -= 1;
        }
    }
}
