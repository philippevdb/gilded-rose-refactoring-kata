package com.gildedrose;

import org.graalvm.compiler.nodes.NodeView;

public class InventoryLifeFactory {

    public static InventoryLifeTemplate getInventoryLife(String name) {
        return ItemType.getItemType(name).getStrategy();
    }

    private enum ItemType {
        ORDINARY("", new DefaultInventoryLifeStrategy()),
        AGED_BRIE("Aged Brie", new AgedBrieStrategy()),
        SULFURAS("Sulfuras, Hand of Ragnaros", new SulfurasStrategy()),
        BACKSTAGE_PASSES("Backstage passes to a TAFKAL80ETC concert",new BackStagePassesStrategy()),
        CONJURED("Conjured", new ConjuredStrategy());

        private String name;
        private InventoryLifeTemplate strategy;

        private ItemType(String name, InventoryLifeTemplate strategy) {
            this.name = name;
            this.strategy = strategy;
        }

        private String getName() {
            return this.name;
        }

        private InventoryLifeTemplate getStrategy() {
            return this.strategy;
        }

        private static ItemType getItemType(String name) {
            for (ItemType itemType : ItemType.values()) {
                if (itemType.getName().equals(name)) {
                    return itemType;
                }
            }
            return ORDINARY;
        }
    }
}
