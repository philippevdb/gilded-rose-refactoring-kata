package com.gildedrose;

public class DefaultInventoryLifeStrategy extends InventoryLifeTemplate {

    @Override
    protected void updateQualityBeforeAging(Item item) {
        decreaseQuality(item);
    }

    @Override
    protected void updateQualityAfterAging(Item item) {
        decreaseQuality(item);
    }
}
