package com.gildedrose;

class GildedRose {
    Item[] items;

    public GildedRose(Item[] items) {
        this.items = items;
    }

    public void updateQuality() {
        for (Item item : items) {
            InventoryLifeTemplate inventoryLife = InventoryLifeFactory.getInventoryLife(item.name);
            inventoryLife.updateQuality(item);
        }
    }

}
